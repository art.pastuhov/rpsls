#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
expose 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Rpsls/Rpsls.Api.csproj", "Rpsls/"]
COPY ["Rpsls.DataLayer/Rpsls.DataLayer.csproj", "Rpsls.DataLayer/"]
COPY ["Rpsls.Services/Rpsls.Services.csproj", "Rpsls.Services/"]
COPY ./*.sln ./
RUN dotnet restore
COPY . .
WORKDIR "/src"
RUN dotnet build -c Release --no-restore

FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Rpsls.Api.dll"]
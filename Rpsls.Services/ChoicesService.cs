﻿using Microsoft.EntityFrameworkCore;
using Rpsls.DataLayer.Entities;
using Rpsls.DataLayer.Repositories;
using Rpsls.Services.Interfaces;

namespace Rpsls.Services
{
    public class ChoicesService: IChoicesService
    {
        private readonly IEntityRepository<Choice> _choicesRepository;

        public ChoicesService(IEntityRepository<Choice> choicesRepository) =>
            _choicesRepository = choicesRepository ?? throw new ArgumentNullException(nameof(choicesRepository));

        public async Task<IEnumerable<Choice>> GetAllChoicesAsync() =>
            await _choicesRepository.GetAll().ToArrayAsync();
    }
}

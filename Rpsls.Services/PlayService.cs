﻿using Rpsls.DataLayer.Entities;
using Rpsls.DataLayer.Repositories;
using Rpsls.Services.DTO;
using Rpsls.Services.Enums;
using Rpsls.Services.Interfaces;

namespace Rpsls.Services
{
    public class PlayService : IPlayService
    {
        private readonly IRandomChoiceService _randomChoiceService;
        private readonly IEntityRepository<GameRule> _gameRuleRepository;
        public PlayService(IRandomChoiceService randomChoiceService,
                           IEntityRepository<GameRule> gameRuleRepository)
        {
            _gameRuleRepository = gameRuleRepository ?? throw new ArgumentNullException(nameof(gameRuleRepository));
            _randomChoiceService = randomChoiceService ?? throw new ArgumentNullException(nameof(randomChoiceService));
        }

        public async Task<List<GameResults>> GetSinglePlayGameResultsAsync(int id)
        {
            var computerResult = new GameResults()
            {
                PlayerName = Constants.DefaultComputerName,
                Choice = (await _randomChoiceService.GetRandomChoiceAsync()).Id,
                Result = PossibleResults.None
            };
            var playerResult = new GameResults()
            {
                PlayerName = Constants.DefaultPlayerName,
                Choice = id,
                Result = PossibleResults.None
            };

            var results = new List<GameResults>() { computerResult, playerResult };
            CalculateGameResults(results);

            return results;
        }

        private void CalculateGameResults(List<GameResults> results)
        {
            for (int i = 0; i < results.Count - 1; i++)
            {
                for (int j = i + 1; j < results.Count; j++)
                {
                    CheckAndMarkPlayersResults(results[i], results[j]);
                }
            }

            CheckAndMarkWinResult(results);
            CheckAndMarkDrawResult(results);
        }

        private static void CheckAndMarkWinResult(List<GameResults> results)
        {
            results.ForEach(x =>
            {
                if (x.Result == PossibleResults.None)
                {
                    x.Result = PossibleResults.Win;
                }
            });
        }

        private static void CheckAndMarkDrawResult(List<GameResults> results)
        {
            if (!results.Any(x => x.Result == PossibleResults.Win) || results.All(x => x.Result == PossibleResults.Win))
            {
                results.ForEach(x => x.Result = PossibleResults.Tie);
            }
        }

        private void CheckAndMarkPlayersResults(GameResults firstResult, GameResults secondResult)
        {
            MarkLoseResults(firstResult, secondResult);
            MarkLoseResults(secondResult, firstResult);
        }

        private void MarkLoseResults(GameResults firstResult, GameResults secondResult)
        {
            if (_gameRuleRepository.GetAll().Any(x => x.ChoiceId == firstResult.Choice && x.LosesToId == secondResult.Choice))
            {
                firstResult.Result = PossibleResults.Lose;
            }
        }
    }
}

﻿using Rpsls.Services.Enums;

namespace Rpsls.Services.DTO
{
    public class GameResults
    {
        public string PlayerName { get; set; }
        public PossibleResults Result { get; set; }
        public int Choice { get; set; }
    }
}

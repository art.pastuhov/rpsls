﻿using Microsoft.EntityFrameworkCore;
using Rpsls.DataLayer.Clients;
using Rpsls.DataLayer.Entities;
using Rpsls.DataLayer.Repositories;
using Rpsls.Services.Interfaces;

namespace Rpsls.Services
{
    public class RandomChoiceService : IRandomChoiceService
    {
        private readonly IEntityRepository<Choice> _choicesRepository;
        private readonly IRandomClient _randomClient;

        public RandomChoiceService(IEntityRepository<Choice> choicesRepository, IRandomClient randomClient)
        {
            _choicesRepository = choicesRepository ?? throw new ArgumentNullException(nameof(choicesRepository));
            _randomClient = randomClient ?? throw new ArgumentNullException(nameof(randomClient));
        }

        public async Task<Choice> GetRandomChoiceAsync()
        {
            var randomValue = await _randomClient.GetRandomChoiceAsync();
            var playebleFigure = await _choicesRepository.GetAll().ToArrayAsync();
            return playebleFigure[randomValue % playebleFigure.Length];
        }
    }
}

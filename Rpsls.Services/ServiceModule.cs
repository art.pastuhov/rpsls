﻿using Autofac;

namespace Rpsls.Services
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(ServiceModule).Assembly)
                .Where(t => t.Name.EndsWith("Service", StringComparison.InvariantCulture)).AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}

﻿using Rpsls.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpsls.Services.Interfaces
{
    public interface IRandomChoiceService
    {
        Task<Choice> GetRandomChoiceAsync();
    }
}

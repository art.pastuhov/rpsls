﻿using Rpsls.Services.DTO;

namespace Rpsls.Services.Interfaces
{
    public interface IPlayService
    {
        Task<List<GameResults>> GetSinglePlayGameResultsAsync(int id);
    }
}

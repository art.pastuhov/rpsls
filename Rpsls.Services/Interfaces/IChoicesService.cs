﻿using Rpsls.DataLayer.Entities;

namespace Rpsls.Services.Interfaces
{
    public interface IChoicesService
    {
        public Task<IEnumerable<Choice>> GetAllChoicesAsync();
    }
}

﻿using Rpsls.Services.Extensions;

namespace Rpsls.Services.Enums
{
    public enum PossibleResults
    {
        None = 0,

        [StringValue("Win")]
        Win = 1,

        [StringValue("Lose")]
        Lose = 2,

        [StringValue("Tie")]
        Tie = 3,
    }
}

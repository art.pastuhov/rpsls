﻿using AutoMapper;
using Rpsls.Controllers.ViewModels;
using Rpsls.DataLayer.Entities;

namespace Rpsls.Configuration.AutoMap
{
    public class ChoicesProfile : Profile
    {
        public ChoicesProfile()
        {
            _ = CreateMap<Choice, ChoiceViewModel>();
        }
    }
}

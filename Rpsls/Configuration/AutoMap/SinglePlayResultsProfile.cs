﻿using AutoMapper;
using Rpsls.Controllers.ViewModels;
using Rpsls.Services.DTO;
using Constants = Rpsls.Services.Constants;

namespace Rpsls.Configuration.AutoMap
{
    public class SinglePlayResultsProfile : Profile
    {
        public SinglePlayResultsProfile()
        {
            _ = CreateMap<List<GameResults>, SinglePlayViewModel>()
                .ForMember(dest => dest.Computer, sor => sor.MapFrom(x => x.First(x => x.PlayerName == Constants.DefaultComputerName).Choice))
                .ForMember(dest => dest.Player, sor => sor.MapFrom(x => x.First(x => x.PlayerName == Constants.DefaultPlayerName).Choice))
                .ForMember(dest => dest.Results, sor => sor.MapFrom(x => x.First(x => x.PlayerName == Constants.DefaultPlayerName).Result));
        }
    }
}

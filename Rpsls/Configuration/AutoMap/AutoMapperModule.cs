﻿using Autofac;
using AutoMapper;

namespace Rpsls.Configuration.AutoMap
{
    public class AutoMapperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(AutoMapperModule).Assembly).As<Profile>();
            builder.RegisterAssemblyTypes(typeof(AutoMapperModule).Assembly).AsClosedTypesOf(typeof(IValueResolver<,,>)).AsSelf();

            builder.Register(context => new MapperConfiguration(cfg => {
                cfg.ConstructServicesUsing(context.Resolve);
                foreach (var profile in context.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();

            builder.Register(c => {
                var context = c.Resolve<IComponentContext>();
                var config = context.Resolve<MapperConfiguration>();
                return config.CreateMapper(context.Resolve);
            }).As<IMapper>().InstancePerLifetimeScope();
        }
    }
}

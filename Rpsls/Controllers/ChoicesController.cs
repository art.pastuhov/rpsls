using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Rpsls.Controllers.ViewModels;
using Rpsls.DataLayer.Entities;
using Rpsls.Services.Interfaces;

namespace Rpsls.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChoicesController : ControllerBase
    {
        private readonly ILogger<ChoicesController> _logger;
        private readonly IChoicesService _choiceService;
        private readonly IMapper _mapper;

        public ChoicesController(ILogger<ChoicesController> logger, IChoicesService choiceService, IMapper mapper)
        {
            _logger = logger;
            _choiceService = choiceService;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ChoiceViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllChoicesAsync()
        {
            var choices = await _choiceService.GetAllChoicesAsync();
            var result = _mapper.Map<IEnumerable<Choice>, List<ChoiceViewModel>> (choices);
            return Ok(result);
        }
    }
}
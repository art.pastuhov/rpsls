﻿using System.ComponentModel.DataAnnotations;

namespace Rpsls.Controllers.ViewModels
{
    public class ChoiceViewModel
    {
        public int Id { get; set; }

        [StringLength(12)]
        public string Name { get; set; }
    }
}

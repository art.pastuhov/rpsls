﻿namespace Rpsls.Controllers.ViewModels
{
    public class SinglePlayViewModel
    {
        public string Results { get; set; }
        public int Player { get; set; }
        public int Computer { get; set; }
    }
}

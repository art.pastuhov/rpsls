﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Rpsls.Controllers.RequestModels;
using Rpsls.Controllers.ViewModels;
using Rpsls.Services.Interfaces;

namespace Rpsls.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlayController : Controller
    {
        private readonly ILogger<ChoicesController> _logger;
        private readonly IPlayService _playService;
        private readonly IMapper _mapper;

        public PlayController(ILogger<ChoicesController> logger, IPlayService playService, IMapper mapper)
        {
            _logger = logger;
            _playService = playService;
            _mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType(typeof(SinglePlayViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllChoicesAsync([FromBody] SinglePlayRequestModel singlePlayRequesModel)
        {
            var gameResults = await _playService.GetSinglePlayGameResultsAsync(singlePlayRequesModel.Player);
            var result = _mapper.Map<SinglePlayViewModel>(gameResults);
            return Ok(result);
        }
    }
}

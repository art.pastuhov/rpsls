﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Rpsls.Controllers.ViewModels;
using Rpsls.DataLayer.Entities;
using Rpsls.Services.Interfaces;

namespace Rpsls.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChoiceController : Controller
    {
        private readonly ILogger<ChoiceController> _logger;
        private readonly IRandomChoiceService _randomChoiceService;
        private readonly IMapper _mapper;

        public ChoiceController(ILogger<ChoiceController> logger, IRandomChoiceService randomChoiceService, IMapper mapper)
        {
            _logger = logger;
            _randomChoiceService = randomChoiceService;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ChoiceViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllChoicesAsync()
        {
            var choice = await _randomChoiceService.GetRandomChoiceAsync();
            var result = _mapper.Map<Choice, ChoiceViewModel>(choice);
            return Ok(result);
        }
    }
}

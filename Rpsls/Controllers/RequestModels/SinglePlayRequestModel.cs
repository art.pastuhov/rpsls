﻿using System.ComponentModel.DataAnnotations;

namespace Rpsls.Controllers.RequestModels
{
    public class SinglePlayRequestModel
    {
        [Required]
        public int Player { get; set; }
    }
}

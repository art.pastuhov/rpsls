﻿using System.Diagnostics;
using Serilog.Events;
using ILogger = Serilog.ILogger;

namespace Rpsls.Middleware
{
    public class ErrorHandlerMiddleware
    {
        private const string MessageTemplate = "HTTP {RequestMethod} {RequestPath} responded {StatusCode} in {Elapsed:0.0000} ms";

        private readonly ILogger _log;

        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(ILogger logger, RequestDelegate next)
        {
            _log = logger;
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            var watchBench = Stopwatch.StartNew();
            try
            {
                await _next(httpContext);
                var elapsedMs = watchBench.ElapsedMilliseconds;

                var statusCode = httpContext.Response?.StatusCode;
                var level = statusCode >= StatusCodes.Status500InternalServerError ? LogEventLevel.Error : LogEventLevel.Information;

                var log = level == LogEventLevel.Error ? LogForErrorContext(httpContext) : _log;
                log.Write(level, MessageTemplate, httpContext.Request.Method, httpContext.Request.Path, statusCode, elapsedMs);

            }
            // Never caught, because `LogException()` returns false.
            catch (Exception ex) when (LogException(httpContext, watchBench.ElapsedMilliseconds, ex)) { }
        }

        private bool LogException(HttpContext httpContext, double elapsedMs, Exception ex)
        {
            LogForErrorContext(httpContext)
                .Error(ex, MessageTemplate, httpContext.Request.Method, httpContext.Request.Path, 500, elapsedMs);

            return false;
        }

        private ILogger LogForErrorContext(HttpContext httpContext)
        {
            var request = httpContext.Request;

            var result = _log
                .ForContext("RequestHeaders", request.Headers.ToDictionary(h => h.Key, h => h.Value.ToString()), destructureObjects: true)
                .ForContext("RequestHost", request.Host)
                .ForContext("RequestProtocol", request.Protocol);

            if (request.HasFormContentType)
            {
                result = result.ForContext("RequestForm", request.Form.ToDictionary(v => v.Key, v => v.Value.ToString()));
            }

            return result;
        }
    }
}

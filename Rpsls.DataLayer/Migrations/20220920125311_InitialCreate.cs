﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Rpsls.DataLayer.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Choices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Choices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ChoiceId = table.Column<int>(type: "integer", nullable: false),
                    LosesToId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameRules", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Choices",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "rock" },
                    { 2, "paper" },
                    { 3, "scissors" },
                    { 4, "lizard" },
                    { 5, "spock" }
                });

            migrationBuilder.InsertData(
                table: "GameRules",
                columns: new[] { "Id", "ChoiceId", "LosesToId" },
                values: new object[,]
                {
                    { 1, 1, 2 },
                    { 2, 1, 5 },
                    { 3, 2, 3 },
                    { 4, 2, 4 },
                    { 5, 3, 1 },
                    { 6, 3, 5 },
                    { 7, 4, 1 },
                    { 8, 4, 3 },
                    { 9, 5, 2 },
                    { 10, 5, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Choices");

            migrationBuilder.DropTable(
                name: "GameRules");
        }
    }
}

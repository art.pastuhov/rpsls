﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Rpsls.DataLayer.Entities;

namespace Rpsls.DataLayer.Configuration
{
    internal class ChoicesConfiguration : IEntityTypeConfiguration<Choice>
    {
        public void Configure(EntityTypeBuilder<Choice> builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Property(b => b.Name).HasMaxLength(12);
            builder.Property(f => f.Id).ValueGeneratedOnAdd();

            builder.HasData(
                new Choice()
                {
                    Id = 1,
                    Name = "rock"
                },
                new Choice()
                {
                    Id = 2,
                    Name = "paper"
                },
                new Choice()
                {
                    Id = 3,
                    Name = "scissors"
                },
                new Choice()
                {
                    Id = 4,
                    Name = "lizard"
                },
                new Choice()
                {
                    Id = 5,
                    Name = "spock"
                }
                );
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Rpsls.DataLayer.Entities;

namespace Rpsls.DataLayer.Configuration
{
    internal class GameRulesConfiguration : IEntityTypeConfiguration<GameRule>
    {
        public void Configure(EntityTypeBuilder<GameRule> builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Property(f => f.Id).ValueGeneratedOnAdd();

            builder.HasData(
                //rock
                new GameRule()
                {
                    Id = 1,
                    ChoiceId = 1,
                    LosesToId = 2
                },
                new GameRule()
                {
                    Id = 2,
                    ChoiceId = 1,
                    LosesToId = 5
                },
                //paper
                new GameRule()
                {
                    Id = 3,
                    ChoiceId = 2,
                    LosesToId = 3
                },
                new GameRule()
                {
                    Id = 4,
                    ChoiceId = 2,
                    LosesToId = 4
                },
                //scissors
                new GameRule()
                {
                    Id = 5,
                    ChoiceId = 3,
                    LosesToId = 1
                },
                new GameRule()
                {
                    Id = 6,
                    ChoiceId = 3,
                    LosesToId = 5
                },
                //lizard
                new GameRule()
                {
                    Id = 7,
                    ChoiceId = 4,
                    LosesToId = 1
                },
                new GameRule()
                {
                    Id = 8,
                    ChoiceId = 4,
                    LosesToId = 3
                },
                //spock
                new GameRule()
                {
                    Id = 9,
                    ChoiceId = 5,
                    LosesToId = 2
                },
                new GameRule()
                {
                    Id = 10,
                    ChoiceId = 5,
                    LosesToId = 4
                }
                );
        }
    }
}

﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Rpsls.DataLayer.Clients;
using Rpsls.DataLayer.Repositories;

namespace Rpsls.DataLayer
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RpslsDbContext>().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterType<RpslsDbContext>().InstancePerLifetimeScope();

            // Register repositories by using Autofac's OpenGenerics feature
            // http://autofac.readthedocs.io/en/latest/register/registration.html#open-generic-components
            builder.RegisterGeneric(typeof(EntityRepository<>)).As(typeof(IEntityRepository<>))
                .InstancePerLifetimeScope();

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables()
                .Build();

            builder.RegisterType<RandomClient>().As<IRandomClient>()
                .WithParameter("url", config["RandomNumberService"]);
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rpsls.DataLayer.Entities
{
    public class GameRule: IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key()]
        public int Id { get; set; }
        public int ChoiceId { get; set; }
        public int LosesToId { get; set; }
    }
}

﻿namespace Rpsls.DataLayer.Entities
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}

﻿namespace Rpsls.DataLayer.Entities
{
    public class Choice : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Rpsls.DataLayer.Configuration;
using Rpsls.DataLayer.Entities;

namespace Rpsls.DataLayer
{
    public class RpslsDbContext : DbContext
    {
        public DbSet<Choice> Choices { get; set; }
        public DbSet<GameRule> GameRules { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = Environment.GetEnvironmentVariable("RpslsDb");
            if (string.IsNullOrEmpty(connectionString))
            {
                IConfiguration config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", true, true)
                    .AddEnvironmentVariables()
                    .Build();
                connectionString = config.GetConnectionString("RpslsDb");
            }
            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException(nameof(modelBuilder));
            }

            modelBuilder.ApplyConfiguration(new ChoicesConfiguration());
            modelBuilder.ApplyConfiguration(new GameRulesConfiguration());
        }

        public virtual void Migrate()
        {
            this.Database.Migrate();
        }
    }
}

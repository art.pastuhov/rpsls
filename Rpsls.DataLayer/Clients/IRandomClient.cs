﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpsls.DataLayer.Clients
{
    public interface IRandomClient
    {
        Task<int> GetRandomChoiceAsync();
    }
}

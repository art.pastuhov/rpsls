﻿using System.Net.Http.Json;

namespace Rpsls.DataLayer.Clients
{
    public class RandomClient : IRandomClient
    {
        private readonly HttpClient _httpClient;
        private readonly string _url;

        public RandomClient(string url)
        {
            _httpClient = new HttpClient();
            _url = url;
        }

        public async Task<int> GetRandomChoiceAsync()
        {
            var random = await _httpClient.GetFromJsonAsync<RandomNumber>(_url);
            if (random == null)
            {
                throw new HttpRequestException($"RandomClient returns bad response");
            }

            return random.random_number;
        }
    }
}

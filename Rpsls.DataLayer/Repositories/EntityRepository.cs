﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Rpsls.DataLayer.Entities;

namespace Rpsls.DataLayer.Repositories
{
    public class EntityRepository<T> : IEntityRepository<T> where T : class, IEntity
    {
        private readonly DbContext _entitiesContext;

        public EntityRepository(DbContext entitiesContext)
        {
            _entitiesContext = entitiesContext ?? throw new ArgumentNullException(nameof(entitiesContext));
        }

        public IQueryable<T> GetAll() => _entitiesContext.Set<T>().AsNoTracking();

        public async Task<T> GetSingleAsync(Guid id)
        {
            var result = await _entitiesContext.Set<T>().FindAsync(id);
            return result;
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate) => GetAll().Where(predicate);

        public void Add(T entity)
        {
            _entitiesContext.Entry(entity);
            _entitiesContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            var dbEntityEntry = _entitiesContext.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public async Task<int> SaveAsync() => await _entitiesContext.SaveChangesAsync();

        public async Task<IDbContextTransaction> BeginTransactionAsync() => await _entitiesContext.Database.BeginTransactionAsync();
    }
}

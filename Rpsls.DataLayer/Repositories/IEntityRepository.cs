﻿using Microsoft.EntityFrameworkCore.Storage;
using Rpsls.DataLayer.Entities;
using System.Linq.Expressions;

namespace Rpsls.DataLayer.Repositories
{
    public interface IEntityRepository<T> where T : class, IEntity
    {
        IQueryable<T> GetAll();

        Task<T> GetSingleAsync(Guid id);

        IQueryable<T> Where(Expression<Func<T, bool>> predicate);

        void Add(T entity);

        void Delete(T entity);

        Task<int> SaveAsync();

        Task<IDbContextTransaction> BeginTransactionAsync();
    }
}
